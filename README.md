<p align="center">
  <img src="https://images.gitee.com/uploads/images/2018/1022/072950_ed8f3896_2071767.png" width="280" height="210" alt="Icon">
  
# PDFunctionLibrary

![java library](https://img.shields.io/badge/type-Libary-gr.svg "type")
![JDK 23](https://img.shields.io/badge/JDK-14-green.svg "SDK")
![Gradle 6.5](https://img.shields.io/badge/Gradle-6.5-04303b.svg "tool")
![Apache 2](https://img.shields.io/badge/license-Apache%202-blue.svg "License")

-- [Java Doc](https://apidoc.gitee.com/fybug/PDFunctionLibrary) --

-------------------------------------------------------------------------------

# 简介

基于日常项目整合的 Java 辅助开发工具包。<br/>

-------------------------------------------------------------------------------

# 包组件
- Annotations  **注释包**
- lang  **常用或未分类的工具包**
- Porcessing  **处理工具包**
- Util  **数据结构包**
- Util.Porcessing  **数据结构相关算法和工具包**


## 使用方法
请导入其 `jar` 文件,文件在 **发行版** 或项目的 **jar** 文件夹下可以找到

> PDF.jar 为不包含源码的包
>
> PDF_all.jar 为包含了源码的包
>
> PDF_sources.jar 为仅包含源码的包

**发行版中可以看到全部版本<br/>项目下的 jar 文件夹是当前最新的每夜版**

可通过 **WIKI** 或者 **doc文档** 深入学习本工具

## 分支说明
**dev-master**：当前的开发分支，可以拿到最新的每夜版 jar

**releases**：当前发布分支，稳定版的源码

-------------------------------------------------------------------------------

### 提供bug反馈或建议

- [码云Gitee](https://gitee.com/PatternDirClean/PDFunctionLibrary/issues)
- [Github](https://github.com/PatternDirClean/PDFunctionLibrary/issues)